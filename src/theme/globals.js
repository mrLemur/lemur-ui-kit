import {fontFamily as fontName} from './presets/typography';

const fontFamily = `font-family: "${fontName}", sans-serif`;

export default `
    * {
        box-sizing: border-box;
    }
    
    body {
        ${fontFamily};

        &.modal_opened {
            overflow: hidden;
        }
    }

    ul {
        list-style: none;
        padding: 0;
        margin: 0;
    }

    keygen, button {
        font-family: "Ubuntu", arial;
    }

    input, textarea, select {
        ${fontFamily};

        &::-webkit-input-placeholder
        &:-moz-placeholder
        &::-moz-placeholder
        &:-ms-input-placeholder {
            ${fontFamily};
        }
    }
`;
