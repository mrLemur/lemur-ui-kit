import {css} from 'styled-components';

const transition = (property, timing = '.2s', easing = 'ease-in-out') => css`
    transition: ${timing} ${easing};
    transition-property: ${property};
`;

export default transition;
