const color = code => props => props.theme.colors[code];

export default color;
