const typography = (type, level) => props =>
    props.theme.presets.typography[type][level];

export default typography;
