const typography = (fontSize, lineHeight, weight = 400) => ({
    fontSize,
    lineHeight,
    weight
});

export const header = {
    1: typography(18, 24, 700),
    2: typography(14, 20, 500)
};

export const paragraph = {
    1: typography(14, 24)
};

export const body = {
    1: typography(12, 16),
    2: typography(12, 20),
    3: typography(12, 16, 500),
    4: typography(12, 20, 500)
};

export const caption = {
    1: typography(10, 14),
    2: typography(10, 16, 700)
};

export default {
    header,
    paragraph,
    body,
    caption
};
