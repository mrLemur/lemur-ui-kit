export default {
    none: 'transparent',
    black100: '#4A4C5A',
    black70: '#5C5E6A',
    black50: '#7D7F8D',
    black30: '#B0B2C0',
    black10: '#E3E5F3',
    black5: '#F5F6FF',
    white: '#FFFFFF',

    accent: '#30BDB5',
    accentDark: '#20A9A0',
    accentBright: '#ACE5E1',
    accentBright2: '#E8F6F6',

    violet: '#BB6BD9',

    accentLink: '#22847F',

    accent2: '#F7BD1F',
    accent2Dark: '#D4A21B',
    accent2Bright: '#FCE5A5',
    accent2Bright2: '#FEF8E9',

    redError: '#EB5757',
    redErrorBright: '#F7BCBC',
    redErrorBright2: '#FDEEEE'
};
