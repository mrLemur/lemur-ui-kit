import {path} from 'ramda';
import {createTypography} from '../generators/typography';

const text = typography => props =>
    createTypography(path(['theme', 'typography', ...typography], props));

export const fontFamily = 'Ubuntu';

export default {
    header: {
        1: text(['header', 1]),
        2: text(['header', 2])
    },
    paragraph: {
        1: text(['paragraph', 1])
    },
    body: {
        1: text(['body', 1]),
        2: text(['body', 2]),
        3: text(['body', 3]),
        4: text(['body', 4])
    },
    caption: {
        1: text(['caption', 1]),
        2: text(['caption', 2])
    }
};
