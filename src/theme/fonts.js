import React from 'react';

import {fontFamily} from './presets/typography';

export default () => (
    <link
        rel="stylesheet"
        href={`https://fonts.googleapis.com/css?family=${fontFamily}:400,500,700&subset=cyrillic`}
    />
);
