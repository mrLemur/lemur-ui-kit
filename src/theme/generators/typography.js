import {css} from 'styled-components';

export const createTypography = options => {
    const {fontSize, lineHeight, weight} = options;

    return css`
        font-size: ${fontSize}px;
        line-height: ${lineHeight}px;
        font-weight: ${weight};
    `;
};
