import {css} from 'styled-components';

const defaultUnits = 'px';

const withUnits = units => value => (value === 0 ? value : `${value}${units}`);

export const width = (width, units = defaultUnits) => css`
    width: ${withUnits(units)(width)};
`;

export const height = (height, units = defaultUnits) => css`
    height: ${withUnits(units)(height)};
`;

export const margin = (margin = [0], units = defaultUnits) => css`
    margin: ${margin.map(withUnits(units)).join(' ')};
`;

export const padding = (padding = [0], units = defaultUnits) => css`
    padding: ${padding.map(withUnits(units)).join(' ')};
`;

export default {
    width,
    height,
    margin,
    padding
};
