import React from 'react';
import PropTypes from 'prop-types';
import {ThemeProvider} from 'styled-components';

import presets from './presets';
import variables from './variables';

const defaultTheme = {
    ...variables,
    presets
};

const CustomThemeProvider = ({children, theme}) => (
    <ThemeProvider theme={{...defaultTheme, ...theme}}>
        {children}
    </ThemeProvider>
);

CustomThemeProvider.propTypes = {
    children: PropTypes.node.isRequired,

    theme: PropTypes.object
};

CustomThemeProvider.defaultProps = {
    theme: {}
};

export {CustomThemeProvider as ThemeProvider};

export {presets};

export {variables};

export default defaultTheme;
