import {path, isNil} from 'ramda';

export default function getFirstDefined(paths, object) {
    let found;

    paths.every(search => {
        const value = path(search.split('.'), object);

        if (!isNil(value)) {
            found = value;

            return false;
        }

        return true;
    });

    return found;
}
