// noinspection ES6CheckImport
import R from 'ramda';

import {getFirstDefined} from './utils';

export default class SchemaInterface {
    static filterOptionsByType = {
        checkbox: ['options'],
        radio: ['options'],
        date: [],
        month: [],
        number: [],
        suggest: ['mode', 'suggest']
    };

    definitions;

    formatters;

    suggests;

    constructor(definitions, formatters, suggests) {
        this.definitions = definitions;
        this.formatters = formatters;
        this.suggests = suggests;
    }

    wrapFormatter = (formatterName, columnData) => {
        const formatter = this.formatters[formatterName];

        if (['object', 'array'].includes(formatterName)) {
            return value => value;
        }

        if (!formatter) {
            throw new Error(
                `Formatter with name '${formatterName}' not found!`
            );
        }

        return value => formatter(value, columnData, this.definitions);
    };

    // Функция, принимающая в себя источник и массив примененных колонок
    // Возвращает конфигурацию таблицы для дальнейшего прокидывания в <Table columns={} />
    generateTableColumns = (sourceName, columns) => {
        const sourceColumns = R.path(
            ['sources', sourceName, 'table', 'columns'],
            this.definitions
        );

        if (!sourceColumns) {
            throw new Error(`No data for source ${sourceName}`);
        }

        return columns.map(column => {
            const sourceColumn = sourceColumns[column];

            if (!sourceColumn) {
                throw new Error(
                    `Field ${column} not found in schema for source ${sourceName}`
                );
            }

            const id = sourceColumn.id || sourceColumn.meta.id;
            const type = sourceColumn.type || sourceColumn.meta.type;
            const title = sourceColumn.title || sourceColumn.meta.title;

            const accessorPath = sourceColumn.accessor
                ? sourceColumn.accessor.split('.')
                : [id];
            const getValue = R.path(accessorPath);
            const formatters = [];

            // noinspection JSUnresolvedVariable
            if (sourceColumn.formatter) {
                // noinspection JSUnresolvedVariable
                formatters.push(
                    this.wrapFormatter(sourceColumn.formatter, sourceColumn)
                );
            }
            if (type) {
                formatters.push(this.wrapFormatter(type, sourceColumn));
            }

            const style =
                sourceColumn.type === 'money' ? {textAlign: 'right'} : null;

            return {
                id: column,
                accessor: R.pipe(
                    getValue,
                    ...formatters
                ),
                Header: title,
                style
            };
        });
    };

    // Генерирует дефолтную конфигурацию колонок
    getDefaultColumnConfiguration(sourceName) {
        const sourceColumns = R.pipe(
            R.path(['sources', sourceName, 'table', 'columns']),
            R.values
        )(this.definitions);

        if (!sourceColumns) {
            throw new Error(`No data for source ${sourceName}`);
        }

        // noinspection JSUnresolvedVariable
        return sourceColumns.map(column => ({
            title: column.title || column.meta.title,
            id: column.id || column.meta.id,
            disabled: column.showAlways,
            checked: column.showByDefault
        }));
    }

    // Генерируем конфигурацию секций карточки
    getSectionsConfig(sourceName) {
        const cardSections = R.path(
            ['sources', sourceName, 'card', 'sections'],
            this.definitions
        );

        return cardSections;
    }

    getFiltersConfig(sourceName) {
        const sourceFilters = R.path(
            ['sources', sourceName, 'filters'],
            this.definitions
        );

        if (!sourceFilters) {
            throw new Error(`No filters for source ${sourceName}`);
        }

        // Get mapping function
        const mapFilterType = R.pipe(
            R.values,
            R.map(filterDefinition => {
                const {type} = filterDefinition;
                const result = {type};

                // Get ID
                result.id = getFirstDefined(
                    ['id', 'meta.id', 'meta.meta.id'],
                    filterDefinition
                );

                // Get title
                result.title = getFirstDefined(
                    ['title', 'meta.title', 'meta.meta.title'],
                    filterDefinition
                );

                // Get defaultValue
                const defaultValue = getFirstDefined(
                    [
                        'defaultValue',
                        'meta.defaultValue',
                        'meta.meta.defaultValue'
                    ],
                    filterDefinition
                );

                result.hasDefaultValue = defaultValue !== undefined;

                if (result.hasDefaultValue) {
                    result.defaultValue = defaultValue;
                }

                // Get filterProps
                result.filterProps =
                    R.pick(
                        SchemaInterface.filterOptionsByType[type],
                        filterDefinition
                    ) || {};

                if (type === 'suggest') {
                    const suggestsUrl = this.suggests[
                        result.filterProps.suggest
                    ];

                    if (suggestsUrl) {
                        result.filterProps.shouldRequestSuggestions = true;
                        result.filterProps.requestSuggestions = value =>
                            fetch(`${suggestsUrl}?q=${value}&limit=40`)
                                .then(res => res.json())
                                .then(res =>
                                    R.is(Array, res.data)
                                        ? res.data.map(item => ({
                                              title: item.name,
                                              value: item.id
                                          }))
                                        : []
                                );
                    } else {
                        result.filterProps.shouldRequestSuggestions = false;
                    }
                }

                return result;
            })
        );

        // Map filters type to array
        return R.map(mapFilterType, sourceFilters);
    }

    getSortingFields(sourceName) {
        const sortingFields = R.pipe(
            R.path(['sources', sourceName, 'sorting', 'fields']),
            R.values
        )(this.definitions);

        if (!sortingFields) {
            throw new Error(`No sorting fields for source ${sourceName}`);
        }

        return sortingFields.map(({id, type, title, meta}) => ({
            id: id || meta.id || meta.meta.id,
            type: type || meta.type || meta.meta.title,
            title: title || meta.title || meta.meta.title
        }));
    }
}
