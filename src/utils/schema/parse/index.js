import parser from 'json-schema-parser';

export default function parse(schema) {
    return parser.parse(schema);
}
