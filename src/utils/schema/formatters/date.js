import {format} from 'date-fns';

export default function dateFormatter(value) {
    const result = format(value, 'dd.MM.yyyy');

    if (result === 'Invalid Date') {
        return '';
    }

    return result;
}
