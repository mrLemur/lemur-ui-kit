import {spacify} from 'utils';

export default function numberFormatter(value) {
    return spacify(value);
}
