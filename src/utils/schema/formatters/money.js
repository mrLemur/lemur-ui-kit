import {spacify} from 'utils';

export default function moneyFormatter(value) {
    if (!value) {
        return '';
    }

    const number = Number(value);
    const decimal = Math.floor(number);
    const float = (number - decimal).toFixed(2);

    return `${spacify(decimal)}.${float.substr(2)}`;
}
