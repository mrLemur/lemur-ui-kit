import {propEq} from 'ramda';

export default function getOptionTitle(value, options) {
    const option = options.find(propEq('value', value));

    if (option) {
        return option.title;
    }

    throw new Error('Option was not found');
}
