import {getOptionTitle} from './utils';

export default function boolFormatter(value, ignore, definition) {
    return getOptionTitle(value, definition.enums.boolean);
}
