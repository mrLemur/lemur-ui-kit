import stringFormatter from './string';
import boolFormatter from './bool';
import enumFormatter from './enum';
import dateFormatter from './date';
import numberFormatter from './number';
import moneyFormatter from './money';

const defaultFormatters = () => ({
    string: stringFormatter,
    bool: boolFormatter,
    enum: enumFormatter,
    date: dateFormatter,
    number: numberFormatter,
    money: moneyFormatter
});

/**
 * Composes default and custom formatters
 * @param customFormatters {Object}
 * @return {Object}
 */
export const createFormatters = (customFormatters = {}) => ({
    ...defaultFormatters(),
    ...customFormatters
});

/**
 * @deprecated - use createFormatters instead
 */
export const formatters = defaultFormatters();
