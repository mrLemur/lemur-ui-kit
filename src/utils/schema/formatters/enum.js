import {getOptionTitle} from './utils';

export default function enumFormatter(value, column) {
    return getOptionTitle(value, column.meta.options);
}
