import source from './source.json';
import table from './table.json';
import filters from './filters.json';
import sorting from './sorting.json';

export default {source, table, filters, sorting};
