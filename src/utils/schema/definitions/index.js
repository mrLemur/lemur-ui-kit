import parse from 'utils/schema/parse';

import example from './example';
import table from './table';
import enums from './enums.json';

const schema = {
    sources: {
        example,
        table
    },
    enums
};

export default parse(schema);
