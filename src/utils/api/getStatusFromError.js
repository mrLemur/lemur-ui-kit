import {path} from 'ramda';

/**
 * Makes attempt to get status string from Error object.
 * Unless returns defaultStatus.
 * @param error {Error}
 * @param defaultStatus {number}
 * @return {number}
 */
const getStatusFromError = (error, defaultStatus = 500) =>
    path(['status'], error) ||
    path(['data', 'status'], error) ||
    path(['response', 'status'], error) ||
    path(['response', 'data', 'status'], error) ||
    defaultStatus;

export default getStatusFromError;
