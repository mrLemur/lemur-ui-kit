import {path} from 'ramda';

/**
 * Makes attempt to get status string from Error object.
 * Unless returns defaultStatus.
 * @param error {Error}
 * @param defaultMessage {string}
 * @return {string}
 */
const getMessageFromError = (error, defaultMessage = 'Unknown error') =>
    path(['message'], error) ||
    path(['data', 'message'], error) ||
    path(['response', 'message'], error) ||
    path(['response', 'data', 'message'], error) ||
    defaultMessage;

export default getMessageFromError;
