import createRules from './createRules';
import formatError from './formatError';

describe('createRule tests:', () => {
    const rulesConfig = {
        email: ['email'],
        username: ['required']
    };

    test('throws if config is undefined', () => {
        expect(() => createRules()).toThrow();
    });

    test('throws if config is not an object', () => {
        expect(() => createRules('string 0_0')).toThrow();
    });

    test('throws if config is an array', () => {
        expect(() => createRules([])).toThrow();
    });

    test('creates an object of rules and validate every rule correctly', () => {
        const rules = createRules(rulesConfig);
        expect(rules.email('not email')).toEqual(formatError('email'));
        expect(rules.username('  ')).toEqual(formatError('required'));
    });

    test('creates an object of rules and transforms every rule to formatError', () => {
        const rules = createRules(rulesConfig).toFormatErrors();
        expect(rules.email).toEqual(formatError('email'));
        expect(rules.username).toEqual(formatError('required'));
    });
});
