import * as rules from './rules';

/**
 * Combines few validators consisting of named rules.
 * Available rules in config:
 *  'email',
 *  'required',
 *  'number',
 *  ['length', length: number],
 *  ['lengthRange', minValue: number, maxValue: number],
 *  ['minLength', minValue: number],
 *  ['maxLength', maxValue: number],
 *  ['equalTo', fieldName: string],
 * @param ruleConfig {[string | [string, ...any]]}
 * @return {function(string, object): object|undefined}
 */
const createRule = ruleConfig => (value, allValues) => {
    /* eslint-disable no-restricted-syntax */
    if (!Array.isArray(ruleConfig)) {
        throw new TypeError('createRule() config should be an array');
    }

    for (const option of ruleConfig) {
        let name;
        let params;

        if (typeof option === 'string') {
            name = option;
        } else {
            [name, ...params] = option;
        }

        const rule = rules[name];

        if (!rule) {
            throw new Error(`There is no rule for key ${name}`);
        }

        const error = rule(value, params, allValues);

        if (error) {
            return error;
        }
    }
    /* eslint-enable no-restricted-syntax */
    return undefined;
};

export default createRule;
