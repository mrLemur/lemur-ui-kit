import createRule from './createRule';
import formatError from './formatError';

/**
 * Replaces every value of object by result of callback
 * @param object {object}
 * @param callback {function}
 * @return {object}
 */
const replaceEveryItem = (object, callback) => {
    if (typeof object !== 'object' || Array.isArray(object)) {
        throw new TypeError('createRules() config should be an object');
    }
    const resultObject = {...object};

    Object.keys(object).forEach(key => {
        resultObject[key] = callback(object[key]);
    });

    return resultObject;
};

/**
 * Takes an object of rules and transform every item to createRule(ruleConfig).
 * Has helper util createRules.prototype.toFormatErrors() that transform every
 * item to formatError().
 * Example of usage:
 * -----------------
 * const rules = createRules({
 *      username: ['required', ['minLength', 6]],
 *      email: ['required', 'email'],
 *      password: ['required', ['lengthRange', 6, 18]],
 *      confirmPassword: ['required', ['equalTo', 'password']]
 *  });
 * -----------------
 * @param config {object}
 * @return {object}
 */
const createRules = config => {
    const resultConfig = replaceEveryItem(config, ruleConfig =>
        createRule(ruleConfig)
    );

    resultConfig.toFormatErrors = () =>
        replaceEveryItem(config, ruleConfig => {
            if (Array.isArray(ruleConfig)) {
                // Pass string to formatError()
                const message = ruleConfig[0];
                return formatError(
                    Array.isArray(message) ? message[0] : message
                );
            }
            return formatError(ruleConfig);
        });
    return resultConfig;
};

export default createRules;
