import formatError from './formatError';

describe('formatError tests:', () => {
    test('formats error without params', () => {
        expect(formatError('required')).toEqual({name: 'required', params: {}});
    });

    test('formats error with params', () => {
        expect(formatError('lengthRange', [4, 6])).toEqual({
            name: 'lengthRange',
            params: [4, 6]
        });
    });
});
