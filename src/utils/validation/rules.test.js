import * as rules from './rules';
import formatError from './formatError';

describe('rules tests:', () => {
    describe('email:', () => {
        const expectedError = formatError('email');

        test('does not work for undefined', () => {
            expect(rules.email(undefined)).toEqual(undefined);
        });

        test('does not work for null', () => {
            expect(rules.email(null)).toEqual(undefined);
        });

        test('works for empty string', () => {
            expect(rules.email('')).toEqual(expectedError);
        });

        test('works for empty strings with spaces', () => {
            expect(rules.email('  ')).toEqual(expectedError);
        });

        test('does not work for email', () => {
            expect(rules.email('info@example.com')).toEqual(undefined);
        });

        test('works for non-email strings', () => {
            expect(rules.email('example.com')).toEqual(expectedError);
        });

        test('works for email strings with spaces', () => {
            expect(rules.email('info@example.com  ')).toEqual(expectedError);
        });
    });

    describe('length:', () => {
        const params = [2];
        const expectedError = formatError('length', {length: 2});

        test('does not throw error if params are not specified', () => {
            expect(rules.length('')).toEqual(undefined);
        });

        test('does not work for undefined', () => {
            expect(rules.length(undefined, params)).toEqual(undefined);
        });

        test('does not work for null', () => {
            expect(rules.length(null, params)).toEqual(undefined);
        });

        test('works for empty string', () => {
            expect(rules.length('', params)).toEqual(expectedError);
        });

        test('does not work for empty strings with spaces', () => {
            expect(rules.length('  ', params)).toEqual(undefined);
        });

        test('does not work if length is equal', () => {
            expect(rules.length('ha', params)).toEqual(undefined);
        });

        test('works if length is less', () => {
            expect(rules.length('h', params)).toEqual(expectedError);
        });

        test('works if length is greater', () => {
            expect(rules.length('has', params)).toEqual(expectedError);
        });
    });

    describe('lengthRange:', () => {
        const params = [2, 4];
        const expectedError = formatError('lengthRange', {max: 4, min: 2});

        test('does not throw error if params are not specified', () => {
            expect(rules.lengthRange('')).toEqual(undefined);
        });

        test('does not throw error if second param is not specified', () => {
            expect(rules.lengthRange('hell', [2])).toEqual(undefined);
        });

        test('does not work for undefined', () => {
            expect(rules.lengthRange(undefined, params)).toEqual(undefined);
        });

        test('does not work for null', () => {
            expect(rules.lengthRange(null, params)).toEqual(undefined);
        });

        test('works for empty string', () => {
            expect(rules.lengthRange('', params)).toEqual(expectedError);
        });

        test('does not work for empty strings with spaces', () => {
            expect(rules.lengthRange('  ', params)).toEqual(undefined);
        });

        test('does not work if length is between range', () => {
            expect(rules.lengthRange('hel', params)).toEqual(undefined);
        });

        test('works if length is less', () => {
            expect(rules.lengthRange('h', params)).toEqual(expectedError);
        });

        test('works if length is greater', () => {
            expect(rules.lengthRange('hello', params)).toEqual(expectedError);
        });
    });

    describe('minLength:', () => {
        const params = [2];
        const expectedError = formatError('minLength', {min: 2});

        test('does not throw error if params are not specified', () => {
            expect(rules.minLength('')).toEqual(undefined);
        });

        test('does not work for undefined', () => {
            expect(rules.minLength(undefined, params)).toEqual(undefined);
        });

        test('does not work for null', () => {
            expect(rules.minLength(null, params)).toEqual(undefined);
        });

        test('works for empty string', () => {
            expect(rules.minLength('', params)).toEqual(expectedError);
        });

        test('does not work for empty strings with spaces', () => {
            expect(rules.minLength('  ', params)).toEqual(undefined);
        });

        test('does not work if length is equal', () => {
            expect(rules.minLength('he', params)).toEqual(undefined);
        });

        test('does not work if length is greater', () => {
            expect(rules.minLength('hello', params)).toEqual(undefined);
        });

        test('works if length is less', () => {
            expect(rules.minLength('h', params)).toEqual(expectedError);
        });
    });

    describe('maxLength:', () => {
        const params = [2];
        const expectedError = formatError('maxLength', {max: 2});

        test('does not throw error if params are not specified', () => {
            expect(rules.maxLength('')).toEqual(undefined);
        });

        test('does not work for undefined', () => {
            expect(rules.maxLength(undefined, params)).toEqual(undefined);
        });

        test('does not work for null', () => {
            expect(rules.maxLength(null, params)).toEqual(undefined);
        });

        test('does not work for empty string', () => {
            expect(rules.maxLength('', params)).toEqual(undefined);
        });

        test('does not work for empty strings with spaces', () => {
            expect(rules.maxLength('  ', params)).toEqual(undefined);
        });

        test('does not work if length is equal', () => {
            expect(rules.maxLength('he', params)).toEqual(undefined);
        });

        test('does not work if length is less', () => {
            expect(rules.maxLength('h', params)).toEqual(undefined);
        });

        test('works if length is greater', () => {
            expect(rules.maxLength('hello', params)).toEqual(expectedError);
        });
    });

    describe('equalTo:', () => {
        const params = ['password'];
        const allValues = {
            password: 'equal-password',
            otherField: ''
        };
        const expectedError = formatError('equalTo', {fieldName: params[0]});

        test('does not throw error if params are not specified', () => {
            expect(rules.equalTo('')).toEqual(
                formatError('equalTo', {fieldName: ''})
            );
        });

        test('does not throw error if allValues are not specified', () => {
            expect(rules.equalTo('', params)).toEqual(expectedError);
        });

        test('does not work for undefined', () => {
            expect(rules.equalTo(undefined, params, allValues)).toEqual(
                undefined
            );
        });

        test('does not work for null', () => {
            expect(rules.equalTo(null, params, allValues)).toEqual(undefined);
        });

        test('does not work for equal value', () => {
            expect(rules.equalTo('equal-password', params, allValues)).toEqual(
                undefined
            );
        });

        test('works for non-equal value', () => {
            expect(rules.equalTo('wrong-password', params, allValues)).toEqual(
                expectedError
            );
        });
    });

    describe('required:', () => {
        const expectedError = formatError('required');

        test('works for undefined', () => {
            expect(rules.required(undefined)).toEqual(expectedError);
        });

        test('works for null', () => {
            expect(rules.required(null)).toEqual(expectedError);
        });

        test('works for boolean (useful for checkboxes)', () => {
            expect(rules.required(false)).toEqual(true);
        });

        test('works for empty string', () => {
            expect(rules.required('')).toEqual(expectedError);
        });

        test('works for empty strings with spaces', () => {
            expect(rules.required('  ')).toEqual(expectedError);
        });

        test('works for NaN', () => {
            expect(rules.required(NaN)).toEqual(expectedError);
        });

        test("doesn't work for numbers", () => {
            expect(rules.required(4)).toEqual(undefined);
        });

        test("doesn't work for non-empty strings", () => {
            expect(rules.required('hello')).toEqual(undefined);
        });
    });

    describe('number:', () => {
        const expectedError = formatError('number');

        test('does not work for undefined', () => {
            expect(rules.number(undefined)).toEqual(undefined);
        });

        test('does not work for null', () => {
            expect(rules.number(null)).toEqual(undefined);
        });

        test('works for empty string', () => {
            expect(rules.number('')).toEqual(expectedError);
        });

        test('works for empty strings with spaces', () => {
            expect(rules.number('  ')).toEqual(expectedError);
        });

        test('does not work for numbers (type String)', () => {
            expect(rules.number('88005553535')).toEqual(undefined);
        });

        test('does not work for numbers (type Number)', () => {
            expect(rules.number(88005553535)).toEqual(undefined);
        });

        test('works for numbers with spaces', () => {
            expect(rules.number('88005553535 ')).toEqual(expectedError);
        });

        test('works for phone numbers', () => {
            expect(rules.number('8 800 555-35-35 ')).toEqual(expectedError);
        });
    });
});
