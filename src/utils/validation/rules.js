import {isNil} from 'ramda';

import isEmail from 'validator/lib/isEmail';
import isNumeric from 'validator/lib/isNumeric';
import isLength from 'validator/lib/isLength';

import formatError from './formatError';

/**
 * Valid Email
 * @param value {string|undefined}
 * @return {object|undefined}
 */
export const email = value => {
    if (isNil(value)) return undefined;

    if (isEmail(`${value}`)) return undefined;

    return formatError('email');
};

/**
 * Concrete field length
 * @param value {string|undefined}
 * @param params {array}
 * @return {object|undefined}
 */
export const length = (value, params = [0]) => {
    if (isNil(value)) return undefined;

    if (isLength(`${value}`, {min: params[0], max: params[0]})) {
        return undefined;
    }
    return formatError('length', {length: params[0]});
};

/**
 * Field length range between two values
 * @param value {string|undefined}
 * @param params {array}
 * @return {object|undefined}
 */
export const lengthRange = (value, params = [0, 0]) => {
    if (isNil(value)) return undefined;

    if (isLength(`${value}`, {min: params[0], max: params[1]})) {
        return undefined;
    }
    return formatError('lengthRange', {min: params[0], max: params[1]});
};

/**
 * Minimum field length
 * @param value {string|undefined}
 * @param params {array}
 * @return {object|undefined}
 */
export const minLength = (value, params = [0]) => {
    if (isNil(value)) return undefined;

    if (isLength(`${value}`, {min: params[0], max: undefined})) {
        return undefined;
    }
    return formatError('minLength', {min: params[0]});
};

/**
 * Maximum field length
 * @param value {string|undefined}
 * @param params {array}
 * @return {object|undefined}
 */
export const maxLength = (value, params = [0]) => {
    if (isNil(value)) return undefined;

    if (isLength(`${value}`, {max: params[0], min: undefined})) {
        return undefined;
    }
    return formatError('maxLength', {max: params[0]});
};

/**
 * Required field
 * @param value {string|undefined}
 * @return {object|undefined}
 */
export const required = value => {
    const error = formatError('required');

    if (isNil(value)) return error;

    if (Number.isNaN(value)) return error;

    // Validation for checkboxes
    if (typeof value === 'boolean') return !value;

    if (typeof value === 'string' && value.trim().length === 0) return error;

    return undefined;
};

/**
 * Two fields' values should be equal
 * @param value {string|undefined}
 * @param params {array}
 * @param allValues {object}
 * @return {object|undefined}
 */
export const equalTo = (value, params = [''], allValues = {}) => {
    const fieldName = params[0];

    if (isNil(value)) return undefined;

    if (typeof fieldName === 'string' && value !== allValues[fieldName]) {
        return formatError('equalTo', {fieldName});
    }

    return undefined;
};

/**
 * Only numbers
 * @param value {string|undefined}
 * @return {object|undefined}
 */
export const number = value => {
    if (isNil(value)) return undefined;

    if (isNumeric(`${value}`, {no_symbols: true})) return undefined;

    return formatError('number');
};
