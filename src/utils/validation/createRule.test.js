import createRule from './createRule';
import formatError from './formatError';

describe('createRule tests:', () => {
    const value = 'value';
    const allValues = {content: 'value'};

    test('throws if config is undefined', () => {
        expect(() => createRule()(value, allValues)).toThrow();
    });

    test('does not throw if config is an empty array', () => {
        expect(createRule([])(value, allValues)).toEqual(undefined);
    });

    test('throws if rule does not exist', () => {
        expect(() => createRule(['wrongRule'])(value, allValues)).toThrow();
    });

    test('validates a simple rule', () => {
        const validate = createRule(['email']);
        expect(validate('not email')).toEqual(formatError('email'));
    });

    test('validates two rules and return first error in sequence', () => {
        const validate = createRule([['maxLength', 16], 'email']);
        expect(validate('not email and greater than 5 symbols')).toEqual(
            formatError('maxLength', {max: 16})
        );
    });

    test('validates two rules and all of them pass the validation', () => {
        const validate = createRule([['maxLength', 16], 'email']);
        expect(validate('info@example.com')).toEqual(undefined);
    });
});
