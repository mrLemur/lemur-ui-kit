export default (name, params = {}) => ({
    name,
    params
});
