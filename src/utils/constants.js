export const headerHeight = 56;
export const tabsHeight = 48;
export const footerHeight = 80;

export const controlBarHeight = 56;

export const shortFiltersHeight = 32;
