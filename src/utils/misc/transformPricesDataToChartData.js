import {clone, sum} from 'ramda';

import colors from 'theme/variables/colors';

const monthsArr = [
    'Янв',
    'Фев',
    'Март',
    'Апр',
    'Май',
    'Июнь',
    'Июль',
    'Авг',
    'Сен',
    'Окт',
    'Ноя',
    'Дек'
];

const fullnameMonthsArr = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
];

export const transformShortToFullMonthName = shortName => {
    // из 'Сен 2018' => 'Сен'
    const month = shortName.split(' ')[0];
    // находим нужный месяц и добавляем к нему год
    return `${fullnameMonthsArr.find(item => item.includes(month))} ${
        shortName.split(' ')[1]
    }`;
};

// Используется если в компонент переданы данные для одной номенклатурной позиции
const transformSingleItemPrice = rawItemPriceData => {
    // Описываем каждую линию, выводимую на график, задаем имя, цвет
    // и привязываемся к данным с помощью префикса
    const singleItemOptionsArr = [
        {
            prefix: 'max',
            name: 'Максимальная',
            color: colors.redError
        },
        {
            prefix: 'arv',
            name: 'Средняя',
            color: colors.accent
        },
        {
            prefix: 'med',
            name: 'Медианная',
            color: colors.redErrorBright
        },
        {
            prefix: 'min',
            name: 'Минимальная',
            color: colors.accentBright
        },
        {
            prefix: 'sqr',
            name: 'Среднеквадр. отклонение',
            color: colors.accent2
        }
    ];

    // Заполняем каждый массив объекта соответствующими данными в виде: {x: 'Апрель 2018', y: 220}
    // Значения y для sqr и arv дополнительно округляем до 2 десятичного знака
    const tempData = {
        max: [],
        arv: [],
        med: [],
        min: [],
        sqr: []
    };

    rawItemPriceData.forEach((item, index) => {
        singleItemOptionsArr.forEach(i => {
            tempData[i.prefix][index] = {
                x: `${monthsArr[Number(item.month) - 1]} ${item.year}`,
                y: item[`price_${i.prefix}`].toFixed(2)
            };
        });
    });

    // Готовим данные для графика, создаем массив объектов вида
    // {id: 'Максимальная', color: '#FFFFFF', data:[{x: 'Апрель 2018', y: 220}, ...]}
    const formattedData = [];

    singleItemOptionsArr.forEach((item, index) => {
        formattedData[index] = {};
        formattedData[index].id = item.name;
        formattedData[index].color = item.color;
        formattedData[index].data = tempData[item.prefix];
    });

    // Создаем объект в котором будем возвращать данные для основного графика,
    // графика для sqrDeviation, и дополнительные параметры, которые позволят
    // эти графики синхронизировать
    const splittedData = {
        prices: undefined,
        sqrDeviation: undefined,
        fill: {
            max: [],
            min: []
        },
        parameters: {
            min: undefined,
            max: undefined
        }
    };
    const prices = clone(formattedData);
    const sqrDeviation = formattedData.slice(4);
    splittedData.prices = prices;

    // Задаем прозрачный цвет для sqr на основном графике, чтобы он попадал в тултип,
    // но не отображался на графике
    splittedData.sqrDeviation = sqrDeviation;
    splittedData.prices[4].color = 'transparent';

    // Задаем цвет для заливки 'коридора цен' - все что под price_max
    splittedData.fill.max = clone(splittedData.prices[0]);
    splittedData.fill.max.color = colors.accent2Bright2;

    // Задаем белый цвет для заливки всего, что под price_min
    splittedData.fill.min = clone(splittedData.prices[3]);
    splittedData.fill.min.color = colors.white;

    // Вычисляем минимальное и максимальное значение на графиках, чтобы задать
    splittedData.parameters.min = Math.min(...tempData.min.map(item => item.y));
    splittedData.parameters.max = Math.max(...tempData.max.map(item => item.y));

    return splittedData;
};

// Используется если в компонент переданы данные для нескольких номенклатурных позиций
const transformMultipleItemPrice = rawPricesData => {
    // Описываем каждую линию, выводимую на график, задаем имя,
    // цвет и привязываемся к данным с помощью префикса
    const multipleItemsOptionsArr = [
        {
            prefix: 'max',
            name: 'Максимальная',
            color: colors.redError
        },
        {
            prefix: 'arv',
            name: 'Средняя',
            color: colors.accent
        },
        {
            prefix: 'min',
            name: 'Минимальная',
            color: colors.accentBright
        }
    ];

    const singleItemPrice = clone(rawPricesData);

    // Вычисляем для каждого месяца min, max и arv, заносим
    // полученное значение в первый переданный массив с данными
    rawPricesData[0].forEach((item, index) => {
        const max = [];
        const arv = [];
        const min = [];
        rawPricesData.forEach(i => {
            max.push(i[index].price_max);
            min.push(i[index].price_min);
            arv.push(i[index].price_arv);
        });
        singleItemPrice[0][index].price_max = Math.max(...max);
        singleItemPrice[0][index].price_min = Math.min(...min);
        singleItemPrice[0][index].price_arv = (sum(arv) / arv.length).toFixed(
            2
        );
    });

    // Заполняем каждый массив объекта соответствующими данными в виде: {x: 'Апрель 2018', y: 220}
    // Значения y для sqr и arv дополнительно округляем до 2 десятичного знака
    const tempData = {
        max: [],
        arv: [],
        min: []
    };

    singleItemPrice[0].forEach((item, index) => {
        multipleItemsOptionsArr.forEach(i => {
            tempData[i.prefix][index] = {
                x: `${monthsArr[Number(item.month) - 1]} ${item.year}`,
                y: item[`price_${i.prefix}`]
            };
        });
    });

    // Готовим данные для графика, создаем массив объектов вида
    // {id: 'Максимальная', color: '#FFFFFF', data:[{x: 'Апрель 2018', y: 220}, ...]}
    const formattedData = [];

    multipleItemsOptionsArr.forEach((item, index) => {
        formattedData[index] = {};
        formattedData[index].id = item.name;
        formattedData[index].color = item.color;
        formattedData[index].data = tempData[item.prefix];
    });

    // Создаем объект в котором будем возвращать данные для основного графика,
    // графика для sqrDeviation,
    // и дополнительные параметры, которые позволят эти графики синхронизировать
    const splittedData = {
        prices: undefined,
        fill: {
            max: [],
            min: []
        },
        parameters: {
            min: undefined,
            max: undefined
        }
    };
    const prices = clone(formattedData);
    splittedData.prices = prices;

    // Задаем цвет для заливки 'коридора цен' - все что под price_max
    splittedData.fill.max = clone(splittedData.prices[0]);
    splittedData.fill.max.color = colors.accent2Bright2;

    // Задаем белый цвет для заливки всего, что под price_min
    splittedData.fill.min = clone(splittedData.prices[2]);
    splittedData.fill.min.color = colors.white;

    // Вычисляем минимальное и максимальное значение на графиках, чтобы задать
    splittedData.parameters.min = Math.min(...tempData.min.map(item => item.y));
    splittedData.parameters.max = Math.max(...tempData.max.map(item => item.y));

    return splittedData;
};

// Проверяем переданные данные, если первый элемент - массив,
// значит передано несколько номенклатурных позиций
export const transformPricesDataToChartData = rawPricesData => {
    const transformedData = rawPricesData[0].length
        ? transformMultipleItemPrice(rawPricesData)
        : transformSingleItemPrice(rawPricesData);
    return transformedData;
};
