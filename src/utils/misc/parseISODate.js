import {parse} from 'date-fns';

const parseISODate = date => parse(date, 'yyyy-MM-dd', new Date());

export default parseISODate;
