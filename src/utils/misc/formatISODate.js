import {format} from 'date-fns';

const formatISODate = date => format(date, 'yyyy-MM-dd');

export default formatISODate;
