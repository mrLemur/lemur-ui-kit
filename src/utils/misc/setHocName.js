const setHocName = (hocName, component) => {
    const childName = component.displayName || component.name || 'unknown';
    return `${hocName}(${childName})`;
};

export default setHocName;
