import React from 'react';

import {Tabs, Link} from 'atoms';

test('Render Tabs component with active tab', () => {
    const wrapper = renderWithContext(
        <Tabs
            items={[
                {
                    title: 'Табы',
                    id: 'tab1',
                    onSelect: () => null
                },
                {
                    title: 'С работой',
                    id: 'tab2',
                    onSelect: () => null
                },
                {
                    title: 'На onSelect',
                    id: 'tab3',
                    onSelect: () => null
                }
            ]}
            activeTab="tab1"
        />
    );
    expect(wrapper).toMatchSnapshot();
});

test('Render Tabs component as a link without active tab', () => {
    const wrapper = renderWithContext(
        <Tabs
            items={[
                {
                    title: 'Ссылка внутренняя',
                    to: '/internal/link/path'
                },
                {
                    title: 'Ссылка внешняя',
                    to: 'http://example.com',
                    external: true
                }
            ]}
        />
    );
    expect(wrapper).toMatchSnapshot();
});

test('Render Tabs component as a link using linkComponent property', () => {
    const wrapper = renderWithContext(
        <Tabs
            items={[
                {
                    title: 'Ссылка внутренняя',
                    to: '/internal/link/path'
                },
                {
                    title: 'Ссылка внешняя',
                    to: 'http://example.com',
                    external: true
                }
            ]}
            linkComponent={Link}
        />
    );
    expect(wrapper).toMatchSnapshot();
});
