import React from 'react';

import {action} from '@storybook/addon-actions';
import {text, object, select} from '@storybook/addon-knobs';
import {compose, withStateHandlers} from 'recompose';

import {Tabs} from 'atoms';

import {Story} from 'helpers';

import readme from './README.md';

const WorkingTabs = compose(
    withStateHandlers(
        {
            activeTab: 'tab1'
        },
        {
            onSelect: () => activeTab => ({
                activeTab
            })
        }
    )
)(Tabs);

const items = [
    {
        title: 'Таб 1',
        id: 'tab1'
    },
    {
        title: 'Таб 2',
        id: 'tab2'
    },
    {
        title: 'Таб 3',
        id: 'tab3'
    },
    {
        title: 'Внешняя ссылка',
        id: 'external-link',
        to: 'http://example.com',
        external: true
    }
];

const component = () => [
    <Story key="playground" title="Playground">
        <Tabs
            items={object('Tabs', items)}
            onSelect={action('onSelect')}
            activeTab={text('Active tab', 'tab1')}
            variant={select('Variant', ['light', 'dark'], 'light')}
        />
    </Story>,
    <Story key="examples" title="Examples">
        <WorkingTabs
            items={items}
            onSelect={action('on-tab-select')}
            variant="dark"
        />
    </Story>
];

export default [
    readme,
    component,
    {
        hidePropTypesFor: [WorkingTabs]
    }
];
