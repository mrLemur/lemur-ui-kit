import * as React from 'react';
import PropTypes from 'prop-types';

import {Wrapper, Tab} from './styled';

export default class Tabs extends React.PureComponent {
    static propTypes = {
        onSelect: PropTypes.func,
        linkComponent: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),

        items: PropTypes.arrayOf(
            PropTypes.shape({
                title: PropTypes.string.isRequired,
                id: PropTypes.string,
                to: PropTypes.string,
                external: PropTypes.bool
            })
        ).isRequired,

        variant: PropTypes.oneOf(['light', 'dark']),
        activeTab: PropTypes.string,
        className: PropTypes.string
    };

    static defaultProps = {
        activeTab: null,
        linkComponent: 'a',
        onSelect: () => null,
        variant: 'light',
        className: ''
    };

    onMouseDown = e => {
        e.persist();

        setTimeout(() => {
            e.target.blur();
        }, 0);
    };

    handleSelect = index => e => {
        const {items, onSelect} = this.props;

        e.preventDefault();

        if (onSelect) {
            const item = items[index];

            onSelect(item.id, item, index, e);
        }
    };

    renderTab = (item, index) => {
        const {linkComponent, activeTab, variant} = this.props;

        const props = {
            key: index,
            active: activeTab === item.id ? 1 : 0,
            onMouseDown: this.onMouseDown,
            variant
        };

        if (item.to) {
            props.as = linkComponent;

            if (item.external) {
                props.href = item.to;
                props.target = '_blank';
                props.rel = 'noopener noreferrer';
            } else {
                props.href = item.to;
                props.to = item.to;
            }
        } else {
            props.onClick = this.handleSelect(index);
        }

        return <Tab {...props}>{item.title}</Tab>;
    };

    render() {
        const {items, variant, className} = this.props;

        return (
            <Wrapper className={className} variant={variant}>
                {items.map(this.renderTab)}
            </Wrapper>
        );
    }
}
