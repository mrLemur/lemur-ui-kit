/* eslint-disable prettier/prettier */

import styled, {css} from 'styled-components';

import {color, typography, transition} from 'theme/mixins';

export const Wrapper = styled.div`
    width: 100%;

    background: ${color('white')};

    ${props =>
        props.variant === 'dark' &&
        css`
            background: ${color('black100')};
        `};

    &&:after {
        content: '';
        display: block;
        border-bottom: 4px solid ${props =>
            props.variant === 'dark' ? color('black70') : color('black5')};
        margin-top: -4px;
    }
`;

const tabStyles = {
    light: {
        normal: css`
            border-bottom-color: ${color('black5')};
            color: ${color('black50')};
        `,
        active: css`
            border-bottom-color: ${color('accent')};
            color: ${color('black100')};
        `
    },
    dark: {
        normal: css`
            border-bottom-color: ${color('black70')};
            color: ${color('black30')};
        `,
        active: css`
            border-bottom-color: ${color('accent')};
            color: ${color('white')};
        `
    }
};

export const Tab = styled.button`
    display: inline-block;
    vertical-align: middle;
    min-width: 80px;

    border: none;
    background: transparent;

    border-bottom: 4px solid;

    padding: 14px 8px 10px;

    outline: none;
    text-align: center;
    text-decoration: none;
    cursor: pointer;
    user-select: none;
    text-align: center;

    ${typography('header', 2)}
    ${transition('color, border-color')}

    ${props =>
        props.active
            ? tabStyles[props.variant].active
            : tabStyles[props.variant].normal}

    &:hover,
    &:focus {
        ${props =>
            props.variant === 'dark'
                ? css`
                      color: ${color('black10')};
                  `
                : css`
                      color: ${color('black100')};
                  `}

    &:active {
        color: ${color('accent')};
        transition: none;
    }
`;
