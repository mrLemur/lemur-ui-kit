/* eslint-disable prettier/prettier */

import styled from 'styled-components';

import {transition} from 'theme/mixins';

export const IconContainer = styled.button.attrs({
    type: 'button'
})`
    position: relative;
    border: none;
    border-radius: 4px;
    outline: none;
    padding: 0;

    cursor: ${props => (props.disabled ? 'default' : 'pointer')};

    width: ${props => props.size === 'xs' && '32px'};
    height: ${props => props.size === 'xs' && '32px'};

    background-color: transparent;

    ${transition('background-color')};

    svg {
        top: 50%;
        left: 50%;
        position: absolute;
        transform: translate(-50%, -50%);

        box-sizing: content-box;
        ${transition('fill')};

        width: ${props => `${props.iconWidth}px`};
        height: ${props => `${props.iconHeight}px`};

        fill: ${props =>
            props.disabled
                ? props.theme.colors.black10
                : props.theme.colors.black30};
    }

    ${props =>
        props.variant === 'fill_active' &&
        !props.disabled &&
        `
        background-color: ${props.theme.colors.white};

        &:hover,
        &:focus {
            background-color: ${props.theme.colors.black5};
        }

        &:active {
            background-color: ${props.theme.colors.black10};
            transition: none;
        }
        
    `} ${props =>
        !props.disabled &&
        `
        &:hover svg, 
        &:focus svg {
            fill: ${props.theme.colors.accent};
        }

        &:active svg {
            fill: ${props.theme.colors.accentDark};
            transition: none;
        }
        `};
`;
