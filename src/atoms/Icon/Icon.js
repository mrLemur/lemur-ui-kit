import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';

import {IconContainer} from './styled';

export default class Icon extends PureComponent {
    static propTypes = {
        iconWidth: PropTypes.number,
        iconHeight: PropTypes.number,

        size: PropTypes.string,
        className: PropTypes.string,

        disabled: PropTypes.bool,
        variant: PropTypes.oneOf(['icon_only', 'fill_active']),

        onClick: PropTypes.func,
        onMouseDown: PropTypes.func,

        children: PropTypes.node.isRequired
    };

    static defaultProps = {
        iconWidth: 16,
        iconHeight: 16,

        size: 'xs',
        className: '',
        variant: 'icon_only',

        onMouseDown: null,
        onClick: null,

        disabled: false
    };

    buttonRef = React.createRef();

    onClick = e => {
        const {onClick} = this.props;

        if (onClick) {
            e.preventDefault();

            onClick(e);
        }
    };

    onMouseDown = e => {
        const {onMouseDown} = this.props;

        if (onMouseDown) {
            onMouseDown(e);
        }

        setTimeout(() => {
            const {current} = this.buttonRef;

            if (current && current.blur) {
                current.blur();
            }
        }, 0);
    };

    render() {
        const {children, variant, ...rest} = this.props;

        return (
            <IconContainer
                {...rest}
                onClick={this.onClick}
                variant={variant}
                onMouseDown={this.onMouseDown}
                ref={this.buttonRef}
            >
                {children}
            </IconContainer>
        );
    }
}
