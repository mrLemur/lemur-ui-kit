import React from 'react';

import {Icon} from 'atoms';

import {Excel} from 'icons';

test('Render Icon with Excel icon', () => {
    const wrapper = renderWithContext(
        <Icon onClick={() => null}>
            <Excel />
        </Icon>
    );

    expect(wrapper).toMatchSnapshot();
});

test('Render Icon with Excel icon and with background', () => {
    const wrapper = renderWithContext(
        <Icon onClick={() => null} variant="fill_active">
            <Excel />
        </Icon>
    );

    expect(wrapper).toMatchSnapshot();
});
