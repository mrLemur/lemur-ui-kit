import React from 'react';
import {Icon} from 'atoms';
import {Excel} from 'icons';
import {Story} from 'helpers';
import {select, boolean, number} from '@storybook/addon-knobs';

import readme from './README.md';

const component = () => [
    <Story key="playground" title="Playground">
        <Icon
            variant={select('Variant icon style', ['icon_only', 'fill_active'])}
            size={select('Size', {
                xs: 'xs'
            })}
            iconWidth={number('Size width', 16)}
            iconHeight={number('Size height', 16)}
            disabled={boolean('Disabled Icon')}
        >
            <Excel />
        </Icon>
    </Story>,

    <Story key="examples" title="Examples Icons">
        <Icon iconWidth={24} iconHeight={24}>
            <Excel />
        </Icon>

        <Icon variant="fill_active">
            <Excel />
        </Icon>

        <Icon variant="fill_active">
            <Excel />
        </Icon>

        <Icon>
            <Excel />
        </Icon>

        <Icon disabled>
            <Excel />
        </Icon>

        <Icon iconWidth={50} iconHeight={50}>
            <Excel />
        </Icon>
    </Story>
];

export default [
    readme,
    component,
    {
        hidePropTypesFor: [Excel],
        showPropTypesFor: [Icon]
    }
];
