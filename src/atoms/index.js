import Link from './Link/Link';
import Tabs from './Tabs/Tabs';
import Icon from './Icon/Icon';

export {Link, Tabs, Icon};
