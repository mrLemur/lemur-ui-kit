import React from 'react';

import {Link} from 'atoms';

test('Render Link with default component', () => {
    const wrapper = renderWithContext(<Link>Hello Test 1</Link>);

    expect(wrapper).toMatchSnapshot();
});

test('Render link with another component', () => {
    const testLink = props => (
        <a href="#" {...props}>
            {props.children} {/* eslint-disable-line */}
        </a>
    );
    const wrapper = renderWithContext(
        <Link component={testLink}>Hello another test 2</Link>
    );

    expect(wrapper).toMatchSnapshot();
});

test('Render Link with bright variant style', () => {
    const wrapper = renderWithContext(
        <Link variant="button_bright">Hello Test 3</Link>
    );

    expect(wrapper).toMatchSnapshot();
});

test('Render Link with inline style ', () => {
    const wrapper = renderWithContext(
        <Link variant="inline_normal">Hello Test 3</Link>
    );

    expect(wrapper).toMatchSnapshot();
});

test('Render Link with inline style #2', () => {
    const wrapper = renderWithContext(
        <Link variant="inline_bold">Hello Test 3</Link>
    );

    expect(wrapper).toMatchSnapshot();
});
