import React from 'react';

import styled from 'styled-components';

import {text, select} from '@storybook/addon-knobs';

import {Link} from 'atoms';

import {Story} from 'helpers';

import readme from './README.md';

const testLink = React.forwardRef((props, ref) => (
    <a href="#" {...props} ref={ref}>
        {props.children} {/* eslint-disable-line */}
    </a>
));

const Background = (
    {background, children} // eslint-disable-line
) => (
    <div style={{background}}>{children}</div> // eslint-disable-line
);

const P = styled.p`
    margin: 0;
    padding: 0;
    ${props => props.theme.presets.typography.body[1]};

    & + & {
        margin-left: 50px;
    }
`;

const component = () => [
    <Story key="playground" title="Playground">
        <Background background="#4A4C5A">
            <Link
                variant={select(
                    'Variant',
                    {
                        accent: 'button_accent',
                        bright: 'button_bright',
                        inlineNormal: 'inline_normal',
                        inlineBold: 'inline_bold'
                    },
                    'button_bright'
                )}
            >
                {text('Text', 'Hello world')}
            </Link>
        </Background>
    </Story>,
    <Story key="examples" title="Examples">
        <Background background="#4A4C5A">
            <Link variant="button_bright" component={testLink}>
                Link
            </Link>
        </Background>

        <Link variant="button_accent" component={testLink}>
            Link
        </Link>

        <Link variant="inline_normal" component={testLink}>
            Inline link
        </Link>

        <Link variant="inline_bold" component={testLink}>
            Inline link
        </Link>
    </Story>,
    <Story key="examplesInText" title="Link in text">
        <P>
            Перейдите по адрессу: {/* eslint-disable-line */}
            <Link variant="inline_bold" component={testLink}>
                ссылка
            </Link>
        </P>

        <P>
            Перейдите по адрессу: {/* eslint-disable-line */}
            <Link variant="inline_normal" component={testLink}>
                ссылка
            </Link>
        </P>
    </Story>
];

export default [
    readme,
    component,
    {
        hidePropTypesFor: [P, Background]
    }
];
