import * as React from 'react';
import PropTypes from 'prop-types';

import {LinkContainer} from './styled';

export default class Link extends React.PureComponent {
    static propTypes = {
        onMouseDown: PropTypes.func,
        variant: PropTypes.oneOf([
            'button_bright',
            'button_accent',
            'inline_normal',
            'inline_bold'
        ]),
        component: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
        children: PropTypes.node.isRequired,
        className: PropTypes.string
    };

    static defaultProps = {
        variant: 'button_accent',
        component: null,
        className: '',
        onMouseDown: () => null
    };

    linkRef = React.createRef();

    onMouseDown = (...args) => {
        const {onMouseDown} = this.props;

        if (onMouseDown) {
            onMouseDown(...args);
        }

        setTimeout(() => {
            const {current} = this.linkRef;

            if (current && current.blur) {
                current.blur();
            }
        }, 0);
    };

    render() {
        const {component, variant, children, ...rest} = this.props;

        return (
            <LinkContainer
                {...rest}
                as={component}
                variant={variant}
                ref={this.linkRef}
                onMouseDown={this.onMouseDown}
            >
                {children}
            </LinkContainer>
        );
    }
}
