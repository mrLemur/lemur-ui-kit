/* eslint-disable prettier/prettier */

import styled from 'styled-components';

import {transition} from 'theme/mixins';

export const LinkContainer = styled.a`
    display: block;
    padding: 12px 16px;

    outline: none;
    text-decoration: none;
    cursor: pointer;
    user-select: none;

    ${props => props.theme.presets.typography.body[1]};

    ${transition('color')}

    color: ${props =>
        props.variant === 'button_bright'
            ? props.theme.colors.white
            : props.theme.colors.accentLink
    };

    ${props => props.variant.includes('inline') && 'padding: 0; display: inline;'};
   
    ${props => props.variant === 'inline_bold' && props.theme.presets.typography.body[3]};

    &:hover,
    &:focus {
        color: ${props => props.theme.colors.accent}
    };

    &:active {
        color: ${props => props.theme.colors.accentDark};
        transition: none;
    }
`;
