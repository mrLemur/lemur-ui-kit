/* eslint-disable prettier/prettier */

import styled from 'styled-components';

import Color from 'color';

export const ColorsWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-content: center;
`;

export const ColorContainer = styled.ul`
`;

export const ColorItem = styled.li.attrs({className: 'item'})`
    display: flex;
    justify-content: center;
    align-items: center;

    color: ${props => {
        const isViolet = props.name === 'violet';
        const isDark = !Color(props.background).isLight();
        
        return isDark || isViolet ? 'white' : 'black';
    }};

    background: ${props => props.background};
    height: 80px;
    padding: 5px;
    width: 160px;
`;
