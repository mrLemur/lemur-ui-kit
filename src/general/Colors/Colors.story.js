import React, {PureComponent} from 'react';

import {storiesOf} from '@storybook/react';
import {withOptions} from '@storybook/addon-options';

import {Story} from 'helpers';

import colors from 'theme/variables/colors';

import {ColorsWrapper, ColorContainer, ColorItem} from './styled';

const blackColors = [];
const accentColors = [];
const errorColors = [];
const anotherColors = [];

Object.keys(colors).forEach(value => {
    if (value.search('black') !== -1) {
        blackColors.push({value, color: colors[value]});
    }
    if (value.search('accent') !== -1) {
        accentColors.push({value, color: colors[value]});
    }
    if (value.search('redError') !== -1) {
        errorColors.push({value, color: colors[value]});
    }
    if (value === 'violet') {
        anotherColors.push({value, color: colors[value]});
    }
    if (value === 'white') {
        anotherColors.push({value, color: colors[value]});
    }
});

class Colors extends PureComponent {
    render() {
        return (
            <ColorsWrapper>
                <ColorContainer>
                    {blackColors.map(item => (
                        <ColorItem key={item.color} background={item.color}>
                            <p>
                                {item.value}
                                <br />
                                {item.color}
                            </p>
                        </ColorItem>
                    ))}
                </ColorContainer>
                <ColorContainer>
                    {accentColors.map(item => (
                        <ColorItem key={item.color} background={item.color}>
                            <p>
                                {item.value}
                                <br />
                                {item.color}
                            </p>
                        </ColorItem>
                    ))}
                </ColorContainer>
                <ColorContainer>
                    {errorColors.map(item => (
                        <ColorItem key={item.color} background={item.color}>
                            <p>
                                {item.value}
                                <br />
                                {item.color}
                            </p>
                        </ColorItem>
                    ))}
                </ColorContainer>
                <ColorContainer>
                    {anotherColors.map(item => (
                        <ColorItem
                            key={item.color}
                            name={item.value}
                            background={item.color}
                        >
                            <p>
                                {item.value}
                                <br />
                                {item.color}
                            </p>
                        </ColorItem>
                    ))}
                </ColorContainer>
            </ColorsWrapper>
        );
    }
}

const component = () => {
    withOptions({showAddonPanel: false});

    return (
        <Story title="Colors">
            <Colors />
        </Story>
    );
};

storiesOf('General', module).add('Colors', component);
