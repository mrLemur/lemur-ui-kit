import {storiesOf} from '@storybook/react';
import {withOptions} from '@storybook/addon-options';
import {withDocs} from 'storybook-readme';

import readme from './README.md';

storiesOf('General', module).add(
    'Welcome',
    withDocs({PreviewComponent: () => null})(readme, () => {
        withOptions({showAddonPanel: false});

        return null;
    })
);
