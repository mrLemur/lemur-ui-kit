import React from 'react';

import {storiesOf} from '@storybook/react';
import {withOptions} from '@storybook/addon-options';

import {Story} from 'helpers';
import {Icon} from 'atoms';
import * as icons from 'icons';
import {Wrapper, IconItem, Label} from './styled';

export default class Icons extends React.PureComponent {
    render() {
        return (
            <React.Fragment>
                <Wrapper>
                    {Object.entries(icons).map(([title, Svg]) => (
                        <IconItem key={title}>
                            <Icon>
                                <Svg />
                            </Icon>

                            <Label>{title}</Label>
                        </IconItem>
                    ))}
                </Wrapper>
            </React.Fragment>
        );
    }
}

const component = () => {
    withOptions({showAddonPanel: false});

    return (
        <Story title="Icons">
            <Icons />
        </Story>
    );
};

storiesOf('General', module).add('Icons', component);
