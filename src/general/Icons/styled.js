import styled from 'styled-components';

import {typography} from 'theme/mixins';

export const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 100%;
`;

export const IconItem = styled.div`
    display: flex;
    align-items: center;
    width: 25%;
    min-width: 150px;
    margin: 8px 0;
`;

export const Label = styled.div`
    ${typography('body', 2)};
`;
