import React from 'react';

import {storiesOf} from '@storybook/react';
import {withOptions} from '@storybook/addon-options';
import {fontFamily as family} from 'theme/presets/typography';

import {Story} from 'helpers';

import {
    Header1,
    Header2,
    Paragraph,
    Body1,
    Body2,
    Body3,
    Body4,
    Caption1,
    Caption2
} from './styled';

export class Typography extends React.PureComponent {
    render() {
        return (
            <React.Fragment>
                <Header1>h1 / {family} / bold / 18 / line-height: 24px</Header1>

                <Header2>
                    h2 / {family} / medium / 14 / line-height: 20px
                </Header2>

                <Paragraph>
                    paragraph / {family} / regular / 14 / line-height: 24px
                </Paragraph>

                <Body1>
                    body1 / {family} / regular / 12 / line-height: 16px
                </Body1>

                <Body2>
                    body2 / {family} / regular / 12 / line-height: 20px
                </Body2>

                <Body3>
                    body3 / {family} / medium / 12 / line-height: 16px
                </Body3>

                <Body4>
                    body4 / {family} / medium / 12 / line-height: 20px
                </Body4>

                <Caption1>
                    caption1 / {family} / regular / 10 / line-height: 14px
                </Caption1>

                <Caption2>
                    caption2 / {family} / bold / 10 / line-height: 16px
                </Caption2>
            </React.Fragment>
        );
    }
}

const component = () => {
    withOptions({showAddonPanel: false});

    return (
        <Story title="Typography">
            <Typography />
        </Story>
    );
};

storiesOf('General', module).add('Typography', component);
