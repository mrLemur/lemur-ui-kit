import styled from 'styled-components';

import {typography} from 'theme/mixins';

export const Header1 = styled.div`
    width: 100%;
    ${typography('header', 1)};
`;

export const Header2 = styled.div`
    width: 100%;
    ${typography('header', 2)};
`;

export const Paragraph = styled.div`
    width: 100%;
    ${typography('paragraph', 1)};
`;

export const Body1 = styled.div`
    width: 100%;
    ${typography('body', 1)};
`;

export const Body2 = styled.div`
    width: 100%;
    ${typography('body', 2)};
`;

export const Body3 = styled.div`
    width: 100%;
    ${typography('body', 3)};
`;

export const Body4 = styled.div`
    width: 100%;
    ${typography('body', 4)};
`;

export const Caption1 = styled.div`
    width: 100%;
    ${typography('caption', 1)};
`;

export const Caption2 = styled.div`
    width: 100%;
    ${typography('caption', 2)};
`;
