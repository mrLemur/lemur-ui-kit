import React from 'react';
import {Provider} from 'react-redux';
import {combineReducers, createStore} from 'redux';
import {reducer as reduxFormReducer} from 'redux-form';
import Enzyme, {shallow, render, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {ThemeProvider} from 'theme';

Enzyme.configure({adapter: new Adapter()});

window.matchMedia =
    window.matchMedia ||
    function() {
        return {
            matches: false,
            addListener: function() {},
            removeListener: function() {}
        };
    };

window.requestAnimationFrame =
    window.requestAnimationFrame ||
    function(callback) {
        setTimeout(callback, 0);
    };

global.shallow = shallow;
global.render = render;
global.mount = mount;

const reducer = combineReducers({
    form: reduxFormReducer // mounted under "form"
});
const store = (window.devToolsExtension
    ? window.devToolsExtension()(createStore)
    : createStore)(reducer);

const wrapWithContext = (fn, children, options) =>
    fn(
        <ThemeProvider>
            <Provider store={store}>{children}</Provider>
        </ThemeProvider>,
        options
    );

global.shallowWithContext = (...args) => wrapWithContext(shallow, ...args);
global.mountWithContext = (...args) => wrapWithContext(mount, ...args);
global.renderWithContext = (...args) => wrapWithContext(render, ...args);
