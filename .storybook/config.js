import React from 'react';
import {Provider} from 'react-redux';

import {configure, addDecorator} from '@storybook/react';

import {createGlobalStyle} from 'styled-components';

import {ThemeProvider} from 'theme';
import Fonts from 'theme/fonts';
import store from './configureStore';

import globalStyles from './globals';

import {collectStories} from './helpers';

const GlobalStyles = createGlobalStyle`
    ${globalStyles}
`;

addDecorator(story => (
    <Provider store={store}>
        <ThemeProvider>
            <div>
                <Fonts />

                <GlobalStyles />

                {story()}
            </div>
        </ThemeProvider>
    </Provider>
));

const atoms = require.context('../src/atoms', true, /\.story\.js$/);
const molecules = require.context('../src/molecules', true, /\.story\.js$/);
const organisms = require.context('../src/organisms', true, /\.story\.js$/);
const forms = require.context('../src/forms', true, /\.story\.js$/);
const general = require.context('../src/general', true, /\.story\.js$/);

function loadStories() {
    general
        .keys()
        .reverse()
        .forEach(file => general(file));

    collectStories('Atoms', atoms, module);
    collectStories('Molecules', molecules, module);
    collectStories('Organisms', organisms, module);
    collectStories('Forms', forms, module);
}

configure(loadStories, module);
