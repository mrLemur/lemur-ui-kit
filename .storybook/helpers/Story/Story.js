import React, {PureComponent} from 'react';
import {reduxForm} from 'redux-form';
import PropTypes from 'prop-types';

import {setDisplayName} from 'recompose';

import {Section, Heading, Content} from './styled';

@setDisplayName('Story')
@reduxForm({form: 'defaultForm'})
export default class Story extends PureComponent {
    static propTypes = {
        title: PropTypes.string.isRequired,
        dark: PropTypes.bool,
        children: PropTypes.oneOfType([PropTypes.node, PropTypes.func])
    };

    static defaultProps = {
        dark: false
    };

    renderChildren() {
        const {children, ...props} = this.props;

        if (typeof children === 'function') {
            return children(props);
        }
        return children;
    }

    render() {
        const {title, dark} = this.props;

        return (
            <Section>
                <Heading>{title}</Heading>
                <Content dark={dark}>{this.renderChildren()}</Content>
            </Section>
        );
    }
}
