/* eslint-disable prettier/prettier */

import styled from 'styled-components';

export const Section = styled.div`
    padding: 0 20px;

    & + & {
        margin-top: 20px;

        border-top: 1px solid #eee;
    }
`;

export const Heading = styled.h2`
    font-family: 'Open Sans', arial;
    font-size: 25px;
    margin-bottom: 20px;
    color: rgb(68, 68, 68);
`;

export const Content = styled.div`
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    
    padding: 10px 20px;

    ${props => props.dark && `
        background-color: rgb(74, 76, 90);
    `}

    > * {
        margin-right: 20px;
        margin-bottom: 20px;
    }
`;

export const Col = styled.div`
    & + & {
        margin-left: 20px;
    }
`;
