import {storiesOf} from '@storybook/react';
import {withInfo} from '@storybook/addon-info';
import {withReadme} from 'storybook-readme';
import {withKnobs} from '@storybook/addon-knobs';
import {withOptions} from '@storybook/addon-options';

import path from 'path';

import Story from './Story/Story';

export default (Category, req, module) => {
    const stories = storiesOf(Category, module)
        .addDecorator(withInfo)
        .addDecorator(withKnobs)
        .addDecorator(
            withOptions({
                name: 'Teleportos UI Kit',
                url: '#',
                addonPanelInRight: true,
                showAddonPanel: true
            })
        );

    req.keys().forEach(filename => {
        const component = filename.split(path.sep)[1];

        const storyConfig = req(filename).default;
        const [storyReadme, storyComponent] = storyConfig;
        const defaultStoryOptions = {
            hidePropTypesFor: [],
            showPropTypesFor: []
        };
        const storyOptions = {...defaultStoryOptions, ...storyConfig[2]};

        stories.add(component, withReadme(storyReadme, storyComponent), {
            info: {
                inline: true,
                header: false,
                source: true,
                maxPropsIntoLine: 1,
                propTablesExclude: [Story, ...storyOptions.hidePropTypesFor],
                propTables: storyOptions.showPropTypesFor
            }
        });
    });
};
