import Story from './Story/Story';
import collectStories from './collectStories';

export {collectStories, Story};
