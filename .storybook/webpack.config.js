const path = require('path');

// Extend the base config

module.exports = {
    module: {
        rules: [
            {
                test: /\.js$/,
                use: ['source-map-loader'],
                enforce: 'pre'
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {name: '[path][name].[ext]'}
                    }
                ]
            }
        ]
    },
    resolve: {
        modules: ['node_modules'],
        alias: {
            atoms: path.resolve(__dirname, '../lib/atoms'),
            molecules: path.resolve(__dirname, '../lib/molecules'),
            organisms: path.resolve(__dirname, '../lib/organisms'),
            forms: path.resolve(__dirname, '../lib/forms'),
            general: path.resolve(__dirname, '../lib/general'),
            theme: path.resolve(__dirname, '../lib/theme'),
            icons: path.resolve(__dirname, '../lib/icons'),
            utils: path.resolve(__dirname, '../lib/utils'),
            types: path.resolve(__dirname, '../lib/types'),
            vendor: path.resolve(__dirname, '../lib/vendor'),
            helpers: path.resolve(__dirname, './helpers'),
            assets: path.resolve(__dirname, './assets')
        }
    },
    node: {
        fs: 'empty'
    }
};
